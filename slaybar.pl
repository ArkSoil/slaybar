#!/usr/bin/perl

use strict;
use warnings;

$| = 1;

sub p_bar{#string, pos, max_pos,type
	my @string_a = split('', $_[0]);
	my $step = int(@string_a*$_[2]/$_[1]);
	return "<span underline='".$_[3]."'>".
		join("",@string_a[0..$step-1]).
		"</span>".
		join("",@string_a[$step..@string_a-1]);

}

sub p_bar2{#string, max1, pos1, max2, pos2
	my @string_a = split('', $_[0]);
	
	my $step1 = int(@string_a*$_[2]/$_[1]);
	my $step2 = int(@string_a*$_[4]/$_[3]);
	my $type = "low";
	my ($double, $single) = ($step1, $step2);

	if($step1 > $step2){
		$type = "single";
		($double, $single) = ($step2, $step1);
	}

	return "<span underline='double'>".
		join("",@string_a[0..$double-1]).
		"</span><span underline='".$type."'>".
		join("",@string_a[$double..$single-1]).
		"</span>".
		join("",@string_a[$single..@string_a-1]);

}

sub get_time{
	my @months = ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

	my @days = ("Sun","Mon","Tue","Wed","Thu","Fri","Sat");

	my @months_days = (31,28,31,30,31,30,31,31,30,31,30,31);

	my ($sec,$min,$hour,$mday,$mon,$year,$wday) = localtime();

	my $time = $hour * 3600 + $min * 60 + $sec;

	my $date = $days[$wday]." ".$months[$mon]." ".sprintf("%02d",$mday).", ".sprintf("%02d",$hour).":".sprintf("%02d",$min).":".sprintf("%02d",$sec);

	return p_bar2($date,86400,$time,$months_days[$mon],$mday);
}

sub get_battery{
	my $battery_fn = "/sys/class/power_supply/BAT0/capacity";
	my $ac_fn = "/sys/class/power_supply/AC/online";

	open my $battery, '<', $battery_fn;
	open my $ac, '<', $ac_fn;

	my $charge = <$battery>;
	my $state = "BAT";
	if(int(<$ac>)){$state = "CHG";}

	close $battery;
	close $ac;

	return p_bar(sprintf("%s: % 3d%%",$state,$charge),100,$charge,"single");

}

sub get_brightness{
	my $max_brightness_fn = "/sys/class/backlight/intel_backlight/max_brightness";
	my $brightness_fn = "/sys/class/backlight/intel_backlight/brightness";

	open my $max_brightness, '<', $max_brightness_fn;
	open my $brightness, '<', $brightness_fn;

	my $max = <$max_brightness>;
	my $state = <$brightness>;

	close $max_brightness;
	close $brightness;

	return p_bar(sprintf("BRI: % 3d%%",int($state/$max*100)),$max,$state,"single");

}

open my $pid_file, '>', "/tmp/slaybar_pid";

print $pid_file $$;

close $pid_file;


my $brightness = get_brightness();

$SIG{USR1} = sub { $brightness = get_brightness{} };

while(){
	print join " | ", ($brightness, get_battery(), get_time());
	
	sleep(1);
}




